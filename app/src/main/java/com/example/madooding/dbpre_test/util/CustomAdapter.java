package com.example.madooding.dbpre_test.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.madooding.dbpre_test.MainActivity;
import com.example.madooding.dbpre_test.R;
import com.example.madooding.dbpre_test.model.DataItem;

import java.util.Date;
import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.example.madooding.dbpre_test.util.Constants.TABLE_NAME;

/**
 * Created by madooding on 11/30/2016 AD.
 */

public class CustomAdapter extends ArrayAdapter<DataItem> {
    private List<DataItem> dataItems;
    private NotesHelper helper = new NotesHelper(getContext());

    public CustomAdapter(Context context, int resource, List<DataItem> objects) {
        super(context, resource, objects);
        dataItems = objects;
    }

    @Override
    public int getCount() {
        return dataItems.size();
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final DataItem dataItem = dataItems.get(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View row = inflater.inflate(R.layout.custom_list_view, null);

        TextView title = (TextView) row.findViewById(R.id.item_title);
        title.setText(dataItem.getTitle());

        TextView date = (TextView) row.findViewById(R.id.item_time);
        String dateStr = (String)DateFormat.format("yyyy-MM-dd hh:mm:ss", new Date(dataItem.getDate()));
        date.setText(dateStr);

        final Button del_btn = (Button) row.findViewById(R.id.del_btn);
        del_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataItems.remove(position);
                CustomAdapter.this.notifyDataSetChanged();
                deleteNote((long) dataItem.getId());
            }
        });

        return row;

    }

    public void deleteNote(long id){
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + "=?", new String[] {Long.toString(id)});
    }
}
