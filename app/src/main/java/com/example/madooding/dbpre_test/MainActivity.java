package com.example.madooding.dbpre_test;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.madooding.dbpre_test.model.DataItem;
import com.example.madooding.dbpre_test.util.CustomAdapter;
import com.example.madooding.dbpre_test.util.NotesHelper;

import java.util.ArrayList;
import java.util.List;

import static com.example.madooding.dbpre_test.util.Constants.*;

public class MainActivity extends AppCompatActivity {

    private NotesHelper helper;
    private static String[] COLUMNS = {_ID, TITLE, DATE};
    private static String ORDER_BY = DATE + " DESC";

    private ListView listView;
    private EditText editText;
    private Button add_btn;

    private List<DataItem> dataItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        helper = new NotesHelper(this);

        dataItems = new ArrayList<>();

        listView = (ListView) findViewById(R.id.list_view);
        editText = (EditText) findViewById(R.id.edit_text);
        add_btn = (Button) findViewById(R.id.add_btn);



        try {
            Cursor cursor = getAllNotes();
            showNotes(cursor);
        }
        finally {
            helper.close();
        }

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               addNote(editText.getText().toString());
               editText.setText(null);
               Cursor cursor = getAllNotes();
               showNotes(cursor);
            }
        });


    }


    private Cursor getAllNotes(){
        SQLiteDatabase db =helper.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, COLUMNS, null, null, null, null, ORDER_BY);

        return cursor;
    }

    private void showNotes(Cursor cursor){
        dataItems.clear();
        while(cursor.moveToNext()){
            dataItems.add(new DataItem(cursor.getInt(0), cursor.getString(1), cursor.getLong(2)));
        }
        listView.setAdapter(new CustomAdapter(this, R.layout.custom_list_view, dataItems));
    }

    private void addNote(String str){
        if(str != null && !str.equals("")) {
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(TITLE, str);
            values.put(DATE, System.currentTimeMillis());
            db.insertOrThrow(TABLE_NAME, null, values);
        }
    }


}
