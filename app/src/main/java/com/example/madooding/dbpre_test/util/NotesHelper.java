package com.example.madooding.dbpre_test.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.example.madooding.dbpre_test.util.Constants.DATE;
import static com.example.madooding.dbpre_test.util.Constants.TABLE_NAME;
import static com.example.madooding.dbpre_test.util.Constants.TITLE;

/**
 * Created by madooding on 11/30/2016 AD.
 */

public class NotesHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "notes_sample.db";
    public NotesHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME +
                    " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                         TITLE + " TEXT NOT NULL, " +
                         DATE +  " INTEGER);"
                );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
