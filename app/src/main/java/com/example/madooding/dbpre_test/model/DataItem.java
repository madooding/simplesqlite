package com.example.madooding.dbpre_test.model;

/**
 * Created by madooding on 11/30/2016 AD.
 */

public class DataItem {
    private int id;
    private String title;
    private long date;

    public DataItem(int id, String title, long date){
        setId(id);
        setTitle(title);
        setDate(date);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
