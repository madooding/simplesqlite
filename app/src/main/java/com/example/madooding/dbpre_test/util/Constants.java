package com.example.madooding.dbpre_test.util;

/**
 * Created by madooding on 11/30/2016 AD.
 */

import android.provider.BaseColumns;

public interface Constants extends BaseColumns {
    public static final String TABLE_NAME = "notes";
    public static final String TITLE = "title";
    public static final String DATE = "date";
}
